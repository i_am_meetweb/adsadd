<?php
Route::get('/', function () {
    return redirect('dashboard');
});

Route::get('dashboard', function (){
    return view('dashboard');
});
