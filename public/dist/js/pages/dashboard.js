$(function () {

    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    });
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();

    /* jQueryKnob */
    $(".knob").knob();

    //SLIMSCROLL FOR CHAT WIDGET
    $('#chat-box').slimScroll({
        height: '250px'
    });

    /* Morris.js Charts */
    // Sales chart
    var area = new Morris.Area({
        element: 'monthly-sales',
        resize: true,
        data: [
            {mon: '2008', value: 20},
            {mon: '2009', value: 10},
            {mon: '2010', value: 15},
            {mon: '2011', value: 25},
            {mon: '2012', value: 40},
            {mon: '2013', value: 50}
        ],
        // The name of the data record attribute that contains x-values.
        xkey: 'mon',
        // A list of names of data record attributes that contain y-values.
        ykeys: ['value'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Sales'],
        hideHover: 'auto'
    });

    //Fix for charts under tabs
    $('.box ul.nav a').on('shown.bs.tab', function () {
        area.redraw();
    });

});
