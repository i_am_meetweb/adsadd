@extends('layouts.layout')
@section('title') Dashboard @endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <!-- ./col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!-- small box -->
                <div class="info-box bg-blue">
                    <span class="info-box-icon">
                        <i class="fa fa-book"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">BOOKINGS</span>
                        <span class="info-box-number">All: <small>&#8358;3,000</small></span>
                        <hr />
                        <span class="info-box-number">Placed: <small>30</small></span>
                        <div class="info-box-footer">
                            <a href="#">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!-- small box -->
                <div class="info-box bg-aqua">
                    <span class="info-box-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">MERCHANTS</span>
                        <span class="info-box-number">Total: <small>30</small></span>
                        <hr />
                        <span class="info-box-number">Active: <small>20</small></span>
                        <div class="info-box-footer">
                            <a href="#">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!-- small box -->
                <div class="info-box bg-green">
                    <span class="info-box-icon">
                        <i class="fa fa-newspaper-o"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">ADSLOTS</span>
                        <span class="info-box-number">Total: <small>5,000</small></span>
                        <hr />
                        <span class="info-box-number">Available: <small>30</small></span>
                        <div class="info-box-footer">
                            <a href="#">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <!-- small box -->
                <div class="info-box bg-teal">
                    <span class="info-box-icon">
                        <i class="fa fa-bullhorn"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">ADVERTISERS</span>
                        <span class="info-box-number">All: <small>100</small></span>
                        <hr />
                        <span class="info-box-number">Active: <small>10</small></span>
                        <div class="info-box-footer">
                            <a href="#">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">

            <div class="col-md-12">
                <div class="box box-solid box-info">
                    <div class="box-header">
                        <h3 class="box-title">Monthly Sales</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" title="Collapse this box"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <!--Chart box-->
                            <div class="col-md-7">
                                <div class="tab-content no-padding">
                                    <!-- Morris chart - Sales -->
                                    <div class="chart tab-pane active" id="monthly-sales" style="position: relative; height: 300px;"></div>
                                </div>
                            </div>
                            <!--Chart box-->

                            <!--Book Item Summary-->

                            <div class="col-md-5">
                                <p class="text-center">
                                    <strong>Booked Item Summary</strong>
                                </p>

                                <div class="progress-group">
                                    <span class="progress-text">Awaiting Approval</span>
                                    <span class="progress-number"><b>160</b>/200</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Published</span>
                                    <span class="progress-number"><b>310</b>/400</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Approved</span>
                                    <span class="progress-number"><b>480</b>/800</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Adslots Awaiting</span>
                                    <span class="progress-number"><b>250</b>/500</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                            </div>

                            <!--Book Item Summary-->

                        </div>
                    </div>

                    <!--Box Footer - Total Revenue-->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <h6 class="text-uppercase text-bold text-center">total revenue:</h6>
                                <h1 class="text-center text-green">&#8358;3,000,000.00</h1>
                            </div>
                            <div class="col-sm-6">
                                <h6 class="text-uppercase text-bold text-center">total bookings</h6>
                                <h1 class="text-green text-center">3,000</h1>
                            </div>
                        </div>
                    </div>
                    <!--Box Footer - Total Revenue-->
                </div>
            </div>

        </div>
        <div class="row">            

            <!--Latest Order-->
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header">
                        <h2 class="box-title text-capitalize">latest orders</h2>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <!--Body-->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                                <!--Table Head-->
                                <tr class="text-bold">
                                    <td class="text-capitalize">ID</td>
                                    <td class="text-capitalize">advertiser</td>
                                    <td class="text-capitalize">channel</td>
                                    <td class="text-capitalize">adslot</td>
                                    <td class="text-capitalize">media house</td>
                                    <td class="text-capitalize">Pub. date</td>
                                    <td class="text-capitalize">Status</td>
                                </tr>
                                <!--Table Head-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-primary">waiting</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                                <!--Table Body-->
                                <tr class="text-muted">
                                    <td class="text-capitalize">
                                        <a href="">1201</a>
                                    </td>
                                    <td class="text-capitalize">
                                        <a href="">Lawal Oladipupo</a></td>
                                    <td class="text-capitalize">online</td>
                                    <td class="text-capitalize"></td>
                                    <td class="text-capitalize">City People</td>
                                    <td class="text-capitalize">Jun 18, 2017</td>
                                    <td class="text-capitalize">
                                        <span class="label label-success">Approved</span>
                                    </td>
                                </tr>
                                <!--Table Body-->

                            </tbody>
                        </table>
                    </div>
                    <!--Body-->

                    <!--Footer-->
                    <div class="box-footer">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">«</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>
                    <!--Footer-->
                </div>
            </div>
            <!--Latest Order-->

            <!--Activity-->
            <div class="col-md-4">
                <div class="box box-solid box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title text-capitalize">activity</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body chat" style="overflow: hidden; overflow-y: scroll; width: auto; height: 450px;">
                        <!-- Conversations are loaded here -->
                        <div class="item">
                            <p class="message">
                                <a href="" class="name">
                                    <small class="text-muted pull-right">
                                        <i class="fa fa-clock-o"></i>Jun 12
                                    </small>
                                    Lawal Oladipupo</a>
                                Free live match to all our new subscribers from the month of June to the end of this year.
                            </p>

                            <div class="attachment">
                                <h4>Attachments:</h4>
                                <p class="filename">Theme-thumbnail-image.jpg</p>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm btn-flat">Open</button>
                                </div>
                            </div>

                        </div>
                        <!-- Conversations are loaded here -->
                        <div class="item">
                            <p class="message">
                                <a href="" class="name">
                                    <small class="text-muted pull-right">
                                        <i class="fa fa-clock-o"></i>Jun 12
                                    </small>
                                    Lawal Oladipupo</a>
                                Free live match to all our new subscribers from the month of June to the end of this year.
                            </p>

                            <div class="attachment">
                                <h4>Attachments:</h4>
                                <p class="filename">Theme-thumbnail-image.jpg</p>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm btn-flat">Open</button>
                                </div>
                            </div>

                        </div>
                        <!-- Conversations are loaded here -->
                        <div class="item">
                            <p class="message">
                                <a href="" class="name">
                                    <small class="text-muted pull-right">
                                        <i class="fa fa-clock-o"></i>Jun 12
                                    </small>
                                    Lawal Oladipupo</a>
                                Free live match to all our new subscribers from the month of June to the end of this year.
                            </p>

                            <div class="attachment">
                                <h4>Attachments:</h4>
                                <p class="filename">Theme-thumbnail-image.jpg</p>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm btn-flat">Open</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <!--Activity-->
        </div>
        <!-- /.row (main row) -->

        <div class="row">
            <!--Latest Approvals-->
            <div class="col-md-4">
                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title text-capitalize">Latest Approvals</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <!--Advert Item-->
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Advert Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV</a>
                                    <span class="product-description">
                                        Get a free Samsung Television...
                                    </span>
                                </div>
                            </li>
                            <!--Advert Item-->
                            <!--Advert Item-->
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Advert Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV</a>
                                    <span class="product-description">
                                        Get a free Samsung Television...
                                    </span>
                                </div>
                            </li>
                            <!--Advert Item-->
                            <!--Advert Item-->
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Advert Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV</a>
                                    <span class="product-description">
                                        Get a free Samsung Television...
                                    </span>
                                </div>
                            </li>
                            <!--Advert Item-->
                            <!--Advert Item-->
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Advert Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV</a>
                                    <span class="product-description">
                                        Get a free Samsung Television...
                                    </span>
                                </div>
                            </li>
                            <!--Advert Item-->
                            <!-- /.item -->
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!--Latest Approvals-->

            <!--Latest Members-->
            <div class="col-md-4">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Members</h3>

                        <div class="box-tools pull-right">
                            <span class="label label-danger">8 New Members</span>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">
                            <li>
                                <img src="dist/img/user1-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Alexander Pierce</a>
                                <span class="users-list-date">Today</span>
                            </li>
                            <li>
                                <img src="dist/img/user8-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Norman</a>
                                <span class="users-list-date">Yesterday</span>
                            </li>
                            <li>
                                <img src="dist/img/user7-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Jane</a>
                                <span class="users-list-date">12 Jan</span>
                            </li>
                            <li>
                                <img src="dist/img/user6-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">John</a>
                                <span class="users-list-date">12 Jan</span>
                            </li>
                            <li>
                                <img src="dist/img/user2-160x160.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Alexander</a>
                                <span class="users-list-date">13 Jan</span>
                            </li>
                            <li>
                                <img src="dist/img/user5-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Sarah</a>
                                <span class="users-list-date">14 Jan</span>
                            </li>
                            <li>
                                <img src="dist/img/user4-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Nora</a>
                                <span class="users-list-date">15 Jan</span>
                            </li>
                            <li>
                                <img src="dist/img/user3-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Nadia</a>
                                <span class="users-list-date">15 Jan</span>
                            </li>
                        </ul>
                        <!-- /.users-list -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="javascript:void(0)" class="uppercase">View All Users</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <!--Latest Members-->

            <!--Recently Added Adslots-->
            <div class="col-md-4">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Recently Added Adslots</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Adslot Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV
                                        <span class="label label-success pull-right text-capitalize">approved</span>
                                        <span class="label label-info pull-right">&#8358;10800</span></a>
                                    <span class="product-description">
                                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                                    </span>
                                </div>
                            </li>
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Adslot Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV
                                        <span class="label label-success pull-right text-capitalize">approved</span>
                                        <span class="label label-info pull-right">&#8358;10800</span></a>
                                    <span class="product-description">
                                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                                    </span>
                                </div>
                            </li>
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Adslot Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV
                                        <span class="label label-success pull-right text-capitalize">approved</span>
                                        <span class="label label-info pull-right">&#8358;10800</span></a>
                                    <span class="product-description">
                                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                                    </span>
                                </div>
                            </li>
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Adslot Image">
                                </div>
                                <div class="product-info">
                                    <a href="" class="product-title">Samsung TV
                                        <span class="label label-success pull-right text-capitalize">approved</span>
                                        <span class="label label-info pull-right">&#8358;10800</span></a>
                                    <span class="product-description">
                                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                                    </span>
                                </div>
                            </li>

                            <!-- /.item -->
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!--Recently Added Adslots-->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection