var elixir = require('laravel-elixir');

elixir(function(mix){
    mix.less('AdminLTE.less', 'public/dist/css/AdminLTE.min.css');
});